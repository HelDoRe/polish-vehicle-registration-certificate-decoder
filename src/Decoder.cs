using System.Text;
using NRV2E;

namespace PolishVehicleRegistrationCertificateDecoder
{
    public class Decoder
    {
        private readonly Base64Decoder base64Decoder;
        private readonly NRV2EDecompressor decompressor;
        private readonly VehicleRegistrationInfoParser parser;

        public Decoder()
        {
            this.base64Decoder = new Base64Decoder();
            this.decompressor = new NRV2EDecompressor();
            this.parser = new VehicleRegistrationInfoParser();
        }

        public VehicleRegistrationInfo Decode(string data)
        {
            var bytes = base64Decoder.Decode(data);
            var output = decompressor.Decompress(bytes);
            var text = Encoding.Unicode.GetString(output);
            var info = parser.Parse(text);
            return info;
        }
    }
}