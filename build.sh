# CLEAN
rm *.nupkg
rm -R src/bin src/obj

# BUILD
dotnet restore src/PolishVehicleRegistrationCertificateDecoder.csproj
msbuild /p:Configuration=Release /t:Clean,Build src/PolishVehicleRegistrationCertificateDecoder.csproj

# PACK
nuget pack PolishVehicleRegistrationCertificateDecoder.nuspec

echo "FINISHED."
